package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"math/rand"
	"os"
	"time"
)

func lineCounter(r io.Reader) (int, error) {
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil
		case err != nil:
			return count, err
		}
	}
}

func lineReader(path string, no int) (string, error) {
	file, err := os.Open(path)
	count := 0
	reader := bufio.NewReader(file)
	var line string

	for count != no {
		line, err = reader.ReadString('\n')
		if err != nil {
			fmt.Println("Error:", err)
		}
		count ++
	}
	return line, nil
}

func slang() string {
	slangs := []string{" Get busy !!", " Go Get It !!", " Grind Grind Grind!!!", " Work your a** off", " Ace it kiddo!!", " Level up your GAME!!!", " Geek Out", " Plus Ultra!!!", " Maimum Effort", " Surpass Your Limits"}
	length := len(slangs)
	index := rand.Intn(length)
	return slangs[index]
}

func main() {
	var r io.Reader
	var err error
	var path string

	flag.StringVar(&path, "file", "./todo", "Name of todo file [string]")
	flag.Parse()

	r, err = os.Open(path)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	count, err := lineCounter(r)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	rand.Seed(time.Now().UnixNano())
	random := rand.Intn(count) + 1
	line, err := lineReader(path, random)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	fmt.Println(slang(), "\n\r", line)
}
