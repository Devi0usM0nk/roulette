# Roulette
It is just a script which takes a todo list file and outputs a random task.

## Installation
```shell
$ go get gitlab.com/Devi0usM0nk/roulette
```
## Usage

```shell
$ roulette --help
$ roulette -file <path of todo list file>
```

## Note
* The format of the todo file must be as follows :-
```Text
Task 1
Task 2
Task 3
...
```
